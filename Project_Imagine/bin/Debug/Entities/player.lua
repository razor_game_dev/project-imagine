return {
	"Player",
	components = {
		Body_Component = {8,24},
		Animation_Component = {
			"player_sheet",
			frames = {
				{4,  0, 8, 14},
				{18, 0, 12,24},
				{36, 0, 8, 24},
				{50, 0, 12,24},
				{68, 0, 8, 24},
				{84, 0, 8, 24},
				{100,0, 8, 24},
				{116,0, 8, 24},
			},
			animations = {
				["Walk-Right"] = {10, {0,1}},
				["Walk-Left" ] = {10, {2,3}},
				["Walk-Down" ] = {10, {4,5}},
				["Walk-Up"   ] = {10, {6,7}},
			},
		},
	},	
}