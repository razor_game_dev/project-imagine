﻿using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Project_Imagine.Components;
using Project_Imagine.World;

namespace Project_Imagine.Systems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Draw, Layer = 0)]
    class Animation_Renderer_System : EntityProcessingSystem
    {
        public Animation_Renderer_System() :base(
                typeof(Animation_Component),typeof(Body_Component)
            )
        {

        }

        public override void Process(Entity entity)
        {
            var coll = entity.GetComponent<Body_Component>();
            var spri = entity.GetComponent<Animation_Component>();

            var current_anim = spri.Animations[spri.current_animaion_id];
            var batch = EntitySystem.BlackBoard.GetEntry<SpriteBatch>("SpriteBatch");


            var current_region = spri.Frames[
                    current_anim.frames[(int)current_anim.Current_Frame]
                ];

            if (spri.Playing)
                current_anim.Current_Frame+=current_anim.Play_Speed * Game_Info.Get.Delta_Time;
            
            if (current_anim.Current_Frame > current_anim.frames.Count())
            {
                if (spri.Loop == false)
                {
                    spri.Playing = false;
                    current_anim.Current_Frame = current_anim.frames.Count()-1;
                }else
                    current_anim.Current_Frame = 0;

            }

            float l = .5f + ((1 / (Project_Imagine.MAP_SIZE / ((coll.Y + coll.Height) / Project_Imagine.MAP_SIZE))) / 8) / 10;

            batch.Draw(
                    spri.Image,
                    coll.Position + spri.Offset,
                    current_region,
                    Color.White,
                    0.0f,
                    Vector2.Zero,
                    new Vector2(1,1),
                    SpriteEffects.None,
                    l
                );

        }
    }
}
