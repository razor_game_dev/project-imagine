﻿using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Project_Imagine.Components;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Project_Imagine.Systems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Draw, Layer = 0)]
    class Sprite_Renderer_System : EntityProcessingSystem
    {
        public Sprite_Renderer_System() : base(
                Aspect.All(
                    typeof(Sprite_Component),
                    typeof(Body_Component))
            )
        {

        }
        
        public override void Process(Entity entity)
        {
            var body = entity.GetComponent<Body_Component>();
            var spri = entity.GetComponent<Sprite_Component>();

            var batch = EntitySystem.BlackBoard.GetEntry<SpriteBatch>("SpriteBatch");

            // calculate depth sorting
            float layer = .5f + ((1 / (Project_Imagine.MAP_SIZE / ((body.Y + body.Height) / Project_Imagine.MAP_SIZE))) / 8) / 10;
            spri.Origin = new Vector2(body.Width / 2, body.Height / 2);

            batch.Draw(
                    spri.Image,
                    body.Position + spri.Offset + body.Size / 2,
                    spri.Region,
                    spri.Fill_Color,
                    body.Rotation + spri.Rotation,
                    spri.Origin,
                    new Vector2(spri.Scale, spri.Scale),
                    spri.Effects,
                    layer
                );
        }
    }
}
