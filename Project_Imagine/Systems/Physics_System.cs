﻿using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;
using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Project_Imagine.Components;
using Project_Imagine.World;
using Microsoft.Xna.Framework;

namespace Project_Imagine.Systems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Update, Layer = 0)]
    class Physics_System : EntityProcessingSystem
    {
        private Physics_World physics_world;
        public Physics_System() : base(
                Aspect.All(
                    typeof(Physics_Body_Component),
                    typeof(Body_Component)
                    )
            )
        {
        }

        public override void Process(Entity entity)
        {
            var physics = entity.GetComponent<Physics_Body_Component>();
            var body    = entity.GetComponent<Body_Component>();
            float dt    = Game_Info.Get.Delta_Time;

            if (physics_world == null)
                physics_world = EntitySystem.BlackBoard.GetEntry<Physics_World>("Physics_World");

            if (physics.Physics_Body == null)
            {
                physics_world.Add_Body(entity);
            }

            body.Position = physics.Physics_Body.shape.Position;
        }
    }
}
