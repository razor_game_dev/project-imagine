﻿using Artemis.Interface;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Project_Imagine.Utils;
using System.Collections.Generic;

namespace Project_Image.Components
{

    class Animation
    {
        public int [] frames;
        public float Play_Speed;
        public float Current_Frame;
        
        public Animation(float _play_speed,int [] _frames)
        {
            Current_Frame = 0;
            Play_Speed = _play_speed;
            frames = _frames;
        }
    }

    class Animation_Component : IComponent
    {

        public Dictionary<string,Animation> Animations;
        public List<Rectangle> Frames;

        private Texture2D image;
        public Texture2D Image { get { return image; } }
        
        public Vector2 Offset { get; set; }

        public float Layer { get; set; }
        public string current_animaion_id;
        public bool Playing { get; set; }

        public bool Loop { get; set; }

        public Animation_Component(string _image,List<Rectangle> frames,KeyValuePair<string, Animation> [] animations)
        {
            image = Content_Manager.Get.Get_Texture(_image);
            Frames = frames;
            current_animaion_id = animations[0].Key;

            Playing = false;
            Layer = .5f;
            Offset = Vector2.Zero;
            Loop = true;

            Animations = new Dictionary<string, Animation>();
            foreach (var anim_key in animations)
            {
                Animations.Add(anim_key.Key, anim_key.Value);
            }
        }
    }
}
