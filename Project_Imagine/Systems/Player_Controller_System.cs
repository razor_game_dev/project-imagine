﻿using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;
using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Project_Imagine.Components;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Project_Imagine.Utils;

namespace Project_Imagine.Systems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Update, Layer = 0)]
    class Player_Controller_System : EntityProcessingSystem
    {
        public Player_Controller_System() : base(
                Aspect.All(
                    typeof(Player_Controller_Component),
                    typeof(Body_Component),
                    typeof(Physics_Body_Component)
                    )
            )
        {

        }

        public override void Process(Entity entity)
        {
            var physics = entity.GetComponent<Physics_Body_Component>();
            var player = entity.GetComponent<Player_Controller_Component>();
            var body = entity.GetComponent<Body_Component>();
            var anim = entity.GetComponent<Animation_Component>();

            var VK_LEFT = Input_Manager.Get.Player_Left();
            var VK_RIGHT = Input_Manager.Get.Player_Right();
            var VK_UP = Input_Manager.Get.Player_Up();
            var VK_DOWN = Input_Manager.Get.Player_Down();

            if (Keyboard.GetState().IsKeyDown(Keys.R))
            {
                physics.Rotation_Velocity += 1;
            }else if (Keyboard.GetState().IsKeyDown(Keys.E))
            {
                physics.Rotation_Velocity -= 1;
            }

            if (VK_LEFT)
            {
                physics.Velocity.X += physics.Speed.X * -1;
                anim.current_animaion_id = "Walk-Left";
                anim.Playing = true;
            }

            if (VK_RIGHT)
            {
                physics.Velocity.X += physics.Speed.X *  1;
                anim.current_animaion_id = "Walk-Right";
                anim.Playing = true;
            }

            if (VK_UP)
            {
                physics.Velocity.Y += physics.Speed.Y * -1;
                anim.current_animaion_id = "Walk-Up";
                anim.Playing = true;
            }

            if (VK_DOWN)
            {
                physics.Velocity.Y += physics.Speed.Y * 1;
                anim.current_animaion_id = "Walk-Down";
                anim.Playing = true;
            }

            if (!VK_UP && !VK_DOWN && !VK_LEFT && !VK_RIGHT)
                anim.Playing = false;

        }
    }
}
