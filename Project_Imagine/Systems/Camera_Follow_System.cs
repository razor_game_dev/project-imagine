﻿using Artemis.Attributes;
using Artemis.Manager;
using Artemis.System;
using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Project_Imagine.Components;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Project_Imagine.World;

namespace Project_Imagine.Systems
{
    [ArtemisEntitySystem(ExecutionType = ExecutionType.Synchronous, GameLoopType = GameLoopType.Update, Layer = 0)]
    class Camera_Follow_System : EntityProcessingSystem
    {
        public Camera_Follow_System() : base(
                Aspect.All(
                    typeof(Camera_Follow_Component),
                    typeof(Body_Component)
                    )
            )
        {
        }

        public override void Process(Entity entity)
        {
            var body = entity.GetComponent<Body_Component>();
            var foll = entity.GetComponent<Camera_Follow_Component>();

            var camera = Project_Imagine.Get_Camera();

            float x_dist = camera.Position.X - body.X - body.Width / 2 + Project_Imagine.WIDTH / 2;
            float y_dist = camera.Position.Y - body.Y - body.Height / 2 + Project_Imagine.HEIGHT / 2;

            float x_pos = (camera.Position.X - (x_dist / 2f) * foll.Damping) + foll.Offset.X;
            float y_pos = (camera.Position.Y - (y_dist / 2f) * foll.Damping) + foll.Offset.Y;

            var VK_LEFT = (Keyboard.GetState().IsKeyDown(Game_Info.Get.game_keys.Camera_Left))      || GamePad.GetState(PlayerIndex.One).ThumbSticks.Right.X < 0;
            var VK_RIGHT = (Keyboard.GetState().IsKeyDown(Game_Info.Get.game_keys.Camera_Right))    || GamePad.GetState(PlayerIndex.One).ThumbSticks.Right.X > 0;
            var VK_UP = (Keyboard.GetState().IsKeyDown(Game_Info.Get.game_keys.Camera_Up))          || GamePad.GetState(PlayerIndex.One).ThumbSticks.Right.Y > 0;
            var VK_DOWN = (Keyboard.GetState().IsKeyDown(Game_Info.Get.game_keys.Camera_Down))      || GamePad.GetState(PlayerIndex.One).ThumbSticks.Right.Y < 0;

            float dt = Game_Info.Get.Delta_Time;

            // handle panning
            if (foll.Can_Look)
            {
                if (VK_LEFT)
                    foll.Offset.X -= foll.Pan_Speed * dt;
                else if (VK_RIGHT)
                    foll.Offset.X += foll.Pan_Speed * dt;
                else if (VK_UP)
                    foll.Offset.Y -= foll.Pan_Speed * dt;
                else if (VK_DOWN)
                    foll.Offset.Y += foll.Pan_Speed * dt;
                else
                {
                    foll.Offset.Y *= foll.Friction;
                    foll.Offset.X *= foll.Friction;
                }
            }

            if (foll.Offset.X >  foll.Max_Dist)
                foll.Offset.X =  foll.Max_Dist;
            if (foll.Offset.X < -foll.Max_Dist)
                foll.Offset.X = -foll.Max_Dist;
            if (foll.Offset.Y >  foll.Max_Dist)
                foll.Offset.Y =  foll.Max_Dist;
            if (foll.Offset.Y < -foll.Max_Dist)
                foll.Offset.Y = -foll.Max_Dist;

            camera.Position = new Vector2(x_pos, y_pos);
            


        }
    }
}
