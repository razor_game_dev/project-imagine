﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.World
{
    struct Game_Keys
    {
        public Keys Camera_Left    { get { return Keys.A; } }
        public Keys Camera_Right   { get { return Keys.D; } }
        public Keys Camera_Up      { get { return Keys.W; } }
        public Keys Camera_Down    { get { return Keys.S; } }
    }

    class Game_Info
    {
        private static Game_Info instance;

        public Game_Keys game_keys;
        public float Delta_Time;
        public long Num_Ticks;
        public float Current_FPS;
        public float Average_FPS;

        // (TODO): make getting input easier
        public bool Is_Input(Keys key)
        {
            return false;
        }

        private Game_Info()
        {
            
        }

        public static Game_Info Get
        {
            get
            {
                if (instance == null)
                    instance = new Game_Info();
                return instance;
            }
        }
    }
}
