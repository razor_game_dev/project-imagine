﻿using Microsoft.Xna.Framework;
using Project_Imagine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.World
{
    enum Shape{
        RECT,
        POLY
    }
    class Physics_Shape : Body_Component
    {
        public Vector2 [] Vertices;
        public Shape Coll_Shape { get; }
        public bool Intersecting { get; set; }

        public string ID { get; set; }

        public float Vert_Left { get { return Vertices.Min(v => v.X); } }
        public float Vert_Right { get { return Vertices.Max(v => v.X); } }
        public float Vert_Top { get { return Vertices.Min(v => v.Y); } }
        public float Vert_Bottom { get { return Vertices.Max(v => v.Y); } }

        public bool Contains(Vector2 point)
        {
            return Contains(point.X, point.Y);
        }

        public bool Contains(float x, float y)
        {
            var intersects = 0;
            var vertices = Vertices;

            for (var i = 0; i < vertices.Length; i++)
            {
                var x1 = vertices[i].X;
                var y1 = vertices[i].Y;
                var x2 = vertices[(i + 1) % vertices.Length].X;
                var y2 = vertices[(i + 1) % vertices.Length].Y;

                if ((y1 <= y && y < y2 || y2 <= y && y < y1) && x < (x2 - x1) / (y2 - y1) * (y - y1) + x1)
                    intersects++;
            }

            return (intersects & 1) == 1;
        }

        public Body_Component Get_Body_From_Vertices()
        {
            var min_x = Vert_Left;
            var min_y = Vert_Top;
            var max_x = Vert_Right;
            var max_y = Vert_Bottom;

            return new Body_Component(
                new Vector2(min_x, min_y),
                new Vector2(max_x - min_x, max_y - min_x)
                );
        }

        public Physics_Shape(Vector2 _position,Vector2 [] _vertices) : base(Vector2.Zero, Vector2.Zero)
        {
            Position    = _position;
            Vertices    = _vertices;
            Coll_Shape  = Shape.POLY;
            Intersecting = false;
            ID = "";
        }

        public Physics_Shape(Vector2 _position, Vector2 _size) : base(_position, _size)
        {
            Coll_Shape = Shape.RECT;
            Intersecting = false;
            ID = "";    
        }
    }
}
