﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LineBatch;
using Project_Imagine.Components;
using Artemis;
using Project_Imagine.Utils;

namespace Project_Imagine.World
{
    enum COLL_SIDE
    {
        NONE,
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT       
    }

    class Physics_Body
    {
        public long UUID;
        public Body_Component          shape;
        public Physics_Body_Component  physics;
    }

    class Physics_World
    {
        List<Physics_Shape> shapes;
        List<Physics_Body> physics_bodies;
        //List<Body_Component>
        GraphicsDevice device;

        public Physics_World(GraphicsDevice _device)
        {
            device = _device;

            shapes = new List<Physics_Shape>();
            physics_bodies = new List<Physics_Body>();

            SpriteBatchEx.GraphicsDevice = _device;

            //shapes.Add(
            //        new Physics_Shape(new Vector2(0,0),new Vector2[]
            //        {
            //            new Vector2(10, 0),
            //            new Vector2(0, 20),
            //            new Vector2(20, 20)
            //        }
            //    ));

            //Add_Rectangle_Collider(new Vector2(64, 64), new Vector2(32, 32));
        }

        public Physics_Shape Get_Nearest_Shape(Vector2 point)
        {
            Physics_Shape shape = shapes[0];

            foreach (var s in shapes)
            {
                if (Vector2.Distance(s.Position, point) < Vector2.Distance(shape.Position, point))
                {
                    shape = s;
                }
            }

            return shape;
        }

        public bool Body_In_Any_Shape(Body_Component body)
        {
            if (shapes.Count == 0) {
                return false;
            };
            
            Physics_Shape shape = Get_Nearest_Shape(body.Mid);

            bool coll = false;
            if (shape.Coll_Shape == Shape.POLY)
            {
                var top_left = Point_In_Shape(shape, body.UL);
                var top_right = Point_In_Shape(shape, body.UR);
                var bottom_left = Point_In_Shape(shape, body.BL);
                var bottom_right = Point_In_Shape(shape, body.BR);

                coll = top_right || top_left || bottom_left || bottom_right;

                shape.Intersecting = coll;
                body.Intersecting = coll;
            }else if (shape.Coll_Shape == Shape.RECT)
            {
                coll = body.Intersects(shape);
            }

            return coll;
        }
        
        public bool Collision_Sat(Body_Component body, Physics_Shape shape)
        {
            bool in_shape = false;
            
            return in_shape;
        }

        public bool Point_In_Shape(Physics_Shape shape,Vector2 point)
        {
            
            bool in_shape = false;
                
            if (shape.Coll_Shape == Shape.POLY)
            {
                in_shape = shape.Contains(point);    
            }else if (shape.Coll_Shape == Shape.RECT)
            {
                in_shape = (point.X > shape.Position.X && point.X < shape.Position.X + shape.Size.X &&
                        point.Y > shape.Position.Y && point.Y < shape.Position.Y + shape.Size.Y);
            }

            shape.Intersecting = in_shape;

            return in_shape;
        }

        public void Destroy_All_Rectangle_Colliders_Of_ID(string id)
        {
            //foreach(var shape in shapes)
            //{
            //    if (shape.ID == id)
            //        shapes.Remove(shape);
            //}
        }

        public void Add_Rectangle_Collider(Vector2 pos, Vector2 size, string id = "")
        {
            var shape = new Physics_Shape(pos, size);
            shape.ID = id;
            shapes.Add(shape);
        }

        public void Add_Body(Entity entity)
        {
            var phy_body = new Physics_Body();
            phy_body.shape      = (Body_Component)entity.GetComponent<Body_Component>();
            phy_body.physics    = (Physics_Body_Component)entity.GetComponent<Physics_Body_Component>();
            phy_body.UUID = entity.UniqueId;
            entity.GetComponent<Physics_Body_Component>().Physics_Body = phy_body;
            physics_bodies.Add(phy_body);
        }

        public void Remove_Body(Entity entity)
        {
            physics_bodies.ForEach(e => {
                if (e.UUID == entity.UniqueId)
                    physics_bodies.Remove(e);
            });
        }

        public Body_Component Get_Physics_Applied_Body(Physics_Body body)
        {
            Body_Component new_body = new Body_Component(body.shape.Position, body.shape.Size);

            new_body.X += body.physics.Vel_X * Game_Info.Get.Delta_Time;
            new_body.Y += body.physics.Vel_Y * Game_Info.Get.Delta_Time;

            return new_body;
        }

        public Physics_Body Get_Nearest_Physics_Body(Vector2 point)
        {
            Physics_Body shape = physics_bodies[0];

            foreach (var s in physics_bodies)
            {
                if (Vector2.Distance(s.shape.Position, point) < Vector2.Distance(shape.shape.Position, point))
                {
                    shape = s;
                }
            }

            return shape;
        }

        private bool Line_Intersection(Vector2 begin_1, Vector2 end_1, Vector2 begin_2, Vector2 end_2)
        {
            if (begin_1.X == end_1.X)
                return !(begin_2.X == end_2.X && begin_1.X != begin_2.X);
            else if (begin_2.X == end_2.X) return true;
            else {
                float m1 = (begin_1.Y - end_1.Y) / (begin_1.X - end_1.X);
                float m2 = (begin_2.Y - end_2.Y) / (begin_2.X - end_2.X);
                return m1 != m2;
            }
        }

        private Physics_Body Body_In_Any_Other_Body(Body_Component A)
        {
            var B_Body = Get_Nearest_Physics_Body(A.Position);
            var B = B_Body.shape;


            

            // change later
            return B_Body;
        }

        public void Tick_Physics_World(GameTime game_time)
        {

            float dt = Game_Info.Get.Delta_Time;

            foreach(var body in physics_bodies)
            {
                //var new_body = Get_Physics_Applied_Body(body);

                // check that the physics makes sense
                
                var body_x = new Body_Component(
                        new Vector2(body.shape.X + body.physics.Velocity.X * dt, body.shape.Y),
                        body.shape.Size
                    );
                var body_y = new Body_Component(
                        new Vector2(body.shape.X, body.shape.Y + body.physics.Velocity.Y * dt),
                        body.shape.Size
                    );

                //Body_In_Any_Shape(body.shape);
                //Body_In_Any_Other_Body(body.shape);

                //bool collide = Collision_Sat(body.shape, Get_Nearest_Shape(body.shape.Position));

                if (Body_In_Any_Shape(body_x))
                {
                    body_x = body.shape;
                }

                if (Body_In_Any_Shape(body_y))
                {
                    body_y = body.shape;
                }

                //new_body.Position.X += body_x.X - body.Position.X;
                //new_body.Position.Y += body_y.Y - body.Position.Y;
                body.shape.Rotation     += body.physics.Rotation_Velocity * dt;
                body.shape.Position.X   += body_x.X - body.shape.Position.X;
                body.shape.Position.Y   += body_y.Y - body.shape.Position.Y;

                body.physics.Velocity *= body.physics.Friction;
                body.physics.Rotation_Velocity *= body.physics.Friction;
                // apply the new position
                //body.shape = new_body;
            }
        }


        public void Draw(SpriteBatch batch)
        {
            if (!Project_Imagine.DEBUGGING) return;

            foreach(var body in physics_bodies)
            {
                var shape = body.shape;
                var color = shape.Intersecting ? Color.Red : Color.LimeGreen;
                
                batch.DrawPolyLine(new Vector2[] {
                    body.shape.UL,
                    body.shape.BL,
                    body.shape.BR,
                    body.shape.UR
                }, color, 1, true);
            }

            foreach(var shape in shapes)
            {
                var color = shape.Intersecting ? Color.Red : Color.LimeGreen;
                if (shape.Coll_Shape == Shape.POLY)
                {
                    batch.DrawPolyLine(shape.Vertices, color, 1, true);
                }
                else
                {
                    batch.DrawLine(shape.Position, new Vector2(shape.Position.X, shape.Position.Y + shape.Size.Y), color,1);
                    batch.DrawLine(shape.Position, new Vector2(shape.Position.X + shape.Size.X, shape.Position.Y), color, 1);

                    batch.DrawLine(new Vector2(shape.Position.X,shape.Position.Y + shape.Size.Y), new Vector2(shape.Position.X + shape.Size.X, shape.Position.Y + shape.Size.Y), color, 1);
                    batch.DrawLine(new Vector2(shape.Position.X + shape.Size.X + 1, shape.Position.Y), new Vector2(shape.Position.X + shape.Size.X + 1, shape.Position.Y + shape.Size.Y), color, 1);
                }
            }
        }
    }
}
