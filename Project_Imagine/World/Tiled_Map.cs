﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Project_Imagine.Components;
using Project_Imagine.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledSharp;

namespace Project_Imagine.World
{
    class Tiled_Map
    {
        TmxMap      map;
        Texture2D   image_sheet;
        Physics_World physics_world;

        int animation_ticker;
        int animation_speed;

        int tile_width;
        int tile_height;

        string file;
        bool auto_reload;
        DateTime last_write_time;

        List<TmxLayer> layers;
        List<Rectangle> tile_regions; 
        // add a container for solids

        // Size in pixels
        public Vector2 Size { get; }
        // map offset from origin
        public Vector2 Offset { get; }

        public Tiled_Map(string _file, Texture2D _image_sheet, Vector2 _offset, Physics_World world, bool _auto_reload = false)
        {
            physics_world = world;

            layers          = new List<TmxLayer>();
            tile_regions    = new List<Rectangle>();

            map     = new TmxMap(_file); // load the map
            file    = _file;
            // (TODO): check if map exists

            // set the size of the map in tiles
            tile_width = map.TileWidth;
            tile_height = map.TileHeight;

            var iw = _image_sheet.Width;
            var ih = _image_sheet.Height;
            for (int y = 0; y < (ih / tile_height); y++)
                for (int x = 0; x < (iw / tile_width); x++)
                    tile_regions.Add(new Rectangle(
                                        x * tile_width, 
                                        y * tile_height, 
                                        tile_width, 
                                        tile_height));

            auto_reload = _auto_reload; // should auto reload map
            image_sheet = _image_sheet; // set the image sheet

            if (auto_reload) last_write_time = File.GetLastWriteTime(file);

            animation_speed = 10; // speed of the animation
            animation_ticker = 1; // current frame ticker

            // set the global map size, used for entity layering
            Project_Imagine.MAP_SIZE = map.Height;

            // generate regions;

            // calculate the size of the map in pixels
            Size = new Vector2(
                map.Width * map.TileWidth,
                map.Height * map.TileHeight);

            Offset = _offset; // set the offset

            // check map properties

            // set layers
            foreach (var layer in map.Layers)
                layers.Add(layer);
            
            Generate_Objects();
        }

        public void Generate_Objects()
        {
            foreach (var group in map.ObjectGroups)
            {
                foreach (var obj in group.Objects)
                {
                    switch (obj.Name)
                    {
                        default:
                            physics_world.Add_Rectangle_Collider(
                                new Vector2(
                                    (float)obj.X,
                                    (float)obj.Y
                                    ),
                                new Vector2(
                                    (float)obj.Width,
                                    (float)obj.Height
                                    ),
                                "Map"
                            );
                            break;
                    }
                }
            }
        }

        public void Reload_Map()
        {
            physics_world.Destroy_All_Rectangle_Colliders_Of_ID("Map");

            layers.Clear();
            map = new TmxMap(file);

            foreach (var layer in map.Layers)
                layers.Add(layer);
            //Generate_Objects();
        }

        public void Update(GameTime game_time)
        {
            // update animation timers
            if (game_time.TotalGameTime.Ticks % animation_speed == 0)
                animation_ticker++;

            if (auto_reload && game_time.TotalGameTime.Ticks % 25 == 0)
            {
                if (File.GetLastWriteTime(file) != last_write_time)
                {
                    Console.WriteLine("Realoading the map");
                    Reload_Map();
                    last_write_time = File.GetLastWriteTime(file);
                }
            }
        }

        // draw the map to the screen
        public void Draw(SpriteBatch batch)
        {
            // Loop through all of the layers

            int current_layer = 0;
            foreach(var layer in layers)
            {
                var color = Color.White;

                if (layer.Properties.ContainsKey("color"))
                {
                    color = Math_Utils.Int32_Hex_String_To_Color(
                            layer.Properties["color"]
                        );
                }

                // loop through all of the tiles in the layer
                foreach (TmxLayerTile _t in layer.Tiles)
                {
                    // check if the tile region index is in range
                    if (_t.Gid != 0 && _t.Gid < tile_regions.Count)
                    {
                        int x            = _t.X; // tile x pos
                        int y            = _t.Y; // tile y pos
                        float draw_layer = Project_Imagine.DEFAULT_TILE_LAYER;
                        var region       = tile_regions[_t.Gid - 1]; // get tile frame

                        int found = -1; // used to see if the tile has specific properties

                        // the position to draw the tile
                        Vector2 draw_pos = new Vector2(
                                Offset.X + x * tile_width,
                                Offset.Y + y * tile_height
                            );

                        // (TODO): try to handle paralax later?


                        if (layer.Properties.ContainsKey("layer"))
                        {
                            float _draw_layer = draw_layer;
                            bool success = float.TryParse(
                                layer.Properties["layer"],
                                out _draw_layer);

                            if (!success)
                            {
                                Console.WriteLine("WARNING: cannot parse layer, check to see if theres a type-o!");
                                draw_layer += 0.1f * current_layer;
                            }
                            else
                                draw_layer = _draw_layer;
                        }else
                        {
                            // (NOTE): may not be correct, it could be
                            // the back to front sorting when we want
                            // front to back rendering

                            // set draw layer += current layer depth
                            draw_layer += 0.1f * current_layer;
                        }

                        // (NOTE): this only handles one tileset, 
                        // in the future we may need to change this
                        int i = 0;
                        foreach (var id in map.Tilesets[0].Tiles)
                        {
                            // check if its the same id
                            if (id.Id == _t.Gid - 1)
                            {
                                found = i;
                                break;
                            }
                            i++;
                        }
                        
                        // check to see if theres a tile with properties
                        if (found > 0 && map.Tilesets[0].Tiles[found] != null)
                        {
                            var tile = map.Tilesets[0].Tiles[found];

                            // check if the tile has an animation attached
                            if (tile.AnimationFrames.Count > 0)
                            {
                                // get the current frame
                                int frame = (int)(animation_ticker % (tile.AnimationFrames.Count));
                                // reset if the frame is over the animation length
                                if (frame > tile.AnimationFrames.Count - 1) frame = 0;

                                var r = tile.AnimationFrames[frame].Id;
                                region = tile_regions[r];
                            }
                        }
                        
                        // finally draw the tile
                        batch.Draw(
                                image_sheet,
                                draw_pos,
                                region,
                                color,
                                0f,
                                Vector2.Zero,
                                1f,
                                SpriteEffects.None,
                                draw_layer
                            );

                    }
                }

                current_layer++;
            }

        }

        // clean up the map
        public void Destroy()
        {

        }
    }
}
