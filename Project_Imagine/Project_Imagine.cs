﻿using Artemis;
using Artemis.System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Project_Imagine.Components;
using Project_Imagine.States;
using Project_Imagine.Utils;
using Project_Imagine.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine
{
    class Project_Imagine : Game
    {
        GraphicsDeviceManager   graphics;
        Game_State_Manager      gsm;
        SpriteBatch             batch;
        EntityWorld             world;
        Physics_World           physics_world;
        Lua_Engine              lua_engine;
        Debugger                debugger;
        FPS_Counter             fps_counter;
       
        public static int WIDTH                 = 800;
        public static int HEIGHT                = 480;
        public static float ENTITY_LAYER        = 0.5f;
        public static int MAP_SIZE              = 64;
        public static float DEFAULT_TILE_LAYER  = 0.4f;
        public static bool DEBUGGING = false;
        public static bool PAUSED = false;
        public static Random RND;

        private static Camera2D camera;
        private static long UUID_Counter = 0;
        private static bool should_exit = false;
        private static Color background_color;

        public Project_Imagine()
        {

            // graphics setup
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = WIDTH;
            graphics.PreferredBackBufferHeight = HEIGHT;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();

            // window setup
            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            Window.AllowAltF4 = true;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // create sprite batch
            batch = new SpriteBatch(GraphicsDevice);

            // create modules
            world           = new EntityWorld();
            gsm             = new Game_State_Manager();
            camera          = new Camera2D(GraphicsDevice.Viewport);
            physics_world   = new Physics_World(GraphicsDevice);
            RND             = new Random();
            lua_engine      = new Lua_Engine();
            debugger        = new Debugger(GraphicsDevice,lua_engine);
            fps_counter     = new FPS_Counter();

            Entity_Assembler.Get.Set_Default_Entity_Path("Entities/");

            Entity_Assembler.Get.Give_Lua_Engine(lua_engine);
            Entity_Assembler.Get.Give_Entity_World(world);

            // set background color
            background_color = new Color(25, 25, 25);

            //add to blackboard
            EntitySystem.BlackBoard.SetEntry<SpriteBatch>("SpriteBatch",batch);
            EntitySystem.BlackBoard.SetEntry<Physics_World>("Physics_World", physics_world);

            world.InitializeAll(
                    System.
                    Reflection.
                    Assembly.
                    GetExecutingAssembly()
                );

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Load textures into content manager
            Content_Manager.Get.Add_Textures(
                    Content.Load<Texture2D>("Tiles_1"),
                    Content.Load<Texture2D>("tile_sheet_new"),
                    Content.Load<Texture2D>("old_tilesheet"),
                    Content.Load<Texture2D>("entity_sheet_1"),
                    Content.Load<Texture2D>("player_sheet")
                );

            Content_Manager.Get.Add_Font("sprite_font",
                    Content.Load<SpriteFont>("sprite_font")
                );

            gsm.Goto_Scene(new Level_State(world, lua_engine,physics_world));
        }

        public static void Exit_Game()
        {
            should_exit = true;
        }

        public static Camera2D Get_Camera()
        {
            return camera;
        }

        public static void Set_Background_Color(Color color)
        {
            background_color = color;
        }

        public static long Get_New_UUID()
        {
            return UUID_Counter++;
        }

        public static void Toggle_Pause()
        {
            PAUSED = !PAUSED;
        }

        protected override void Update(GameTime game_time)
        {

            if (should_exit || Input_Manager.Get.Quit_Button()) { 
                Exit();
                return;
            }
            
            Game_Info.Get.Delta_Time = game_time.ElapsedGameTime.Milliseconds / 1000f;
            Game_Info.Get.Num_Ticks = (int)game_time.ElapsedGameTime.TotalSeconds;

            fps_counter.Update(Game_Info.Get.Delta_Time);

            Game_Info.Get.Current_FPS = fps_counter.CurrentFramesPerSecond;
            Game_Info.Get.Average_FPS = fps_counter.AverageFramesPerSecond;
            
            this.Window.Title = "Project Image | FPS: " + Game_Info.Get.Current_FPS + "| DT: " + Game_Info.Get.Delta_Time;

            if (!PAUSED)
            {
                physics_world.Tick_Physics_World(game_time);
                world.Update();
                gsm.Update(game_time);
            }

            debugger.Update(game_time);

            base.Update(game_time);
        }

        protected override void Draw(GameTime game_time)
        {
            GraphicsDevice.Clear(background_color);

            // get the view matrix
            var camera_view_matrix = camera.Get_View_Matrix();

            // GAME DRAW
            batch.Begin(
                SpriteSortMode.FrontToBack,
                BlendState.NonPremultiplied, 
                SamplerState.PointClamp, 
                DepthStencilState.None,
                null,
                null,
                camera_view_matrix
                );

            world.Draw();
            gsm.Draw(batch);

            batch.End();

            // DEBUG DRAW
            batch.Begin(
                SpriteSortMode.FrontToBack,
                BlendState.NonPremultiplied,
                SamplerState.PointClamp,
                DepthStencilState.None,
                null,
                null,
                camera_view_matrix
                );

            physics_world.Draw(batch);
            
            batch.End();

            // GUI DRAW
            batch.Begin(
                SpriteSortMode.FrontToBack,
                BlendState.NonPremultiplied,
                SamplerState.PointClamp,
                DepthStencilState.None,
                null,
                null,
                null
                );

            debugger.Draw(batch);
            batch.End();


            base.Draw(game_time);
        }
    }
}
