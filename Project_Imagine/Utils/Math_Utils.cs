﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    class Math_Utils
    {
        public static Color Int32_Hex_String_To_Color(string hex_string)
        {
            Color color;
            int hex_number = Convert.ToInt32(hex_string, 16);
            color = new Color(
                    (hex_number & 0xff0000) >> 16,
                    (hex_number & 0xff00)   >> 8,
                    (hex_number & 0xff)
                    );
            return color;
        }
    }
}
