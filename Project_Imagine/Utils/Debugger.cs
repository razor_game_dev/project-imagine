﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    class Debugger
    {
        GraphicsDevice device;
        Texture2D rect;
        KbHandler keyboard_handler;
        Lua_Engine lua_engine;

        List<string> last_100_commands;
        int command_list_pointer;

        string command_text;

        public Debugger(GraphicsDevice _device, Lua_Engine _lua_engine)
        {
            lua_engine  = _lua_engine;
            device      = _device;

            last_100_commands = new List<string>();
            command_list_pointer = 0;

            rect = new Texture2D(device, 32, 32);
            Color[] data = new Color[32 * 32];
            for (int i = 0; i < data.Length; i++) data[i] = new Color(25,25,25,.5f);
            rect.SetData(data);

            command_text = "";

            keyboard_handler = new KbHandler();
            keyboard_handler.Add_Keypressed_Callback(On_Keypressed);
            keyboard_handler.Add_Keyreleased_Callback(On_Keyreleased);
        }

        public bool On_Keypressed(Keys key)
        {

            return true;
        }

        public bool On_Keyreleased(Keys key)
        {
            if (key == Keys.Up)
                command_list_pointer--;

            if (key == Keys.Q) Project_Imagine.DEBUGGING = !Project_Imagine.DEBUGGING;

            if (key == Keys.OemSemicolon && keyboard_handler.LControl)
            {
                Project_Imagine.Toggle_Pause();
                command_text = keyboard_handler.tekst = "";
            }

            return true;
        }

        public void Update(GameTime game_time)
        {
            keyboard_handler.Update();

            if (command_list_pointer >= last_100_commands.Count - 1) command_list_pointer = 0;

            if (Project_Imagine.PAUSED)
            {
                command_text = keyboard_handler.tekst;
            }
        
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && command_text.Length > 0) Parse_Command();
        }

        public void Parse_Command()
        {
            last_100_commands.Add(command_text);

            var command = last_100_commands[last_100_commands.Count - 1];

            var tokens = command.Split(new char[] { ' ' });
            Console.WriteLine(command);

            if (tokens[0][0] == ';')
            {
                tokens[0] = tokens[0].Substring(1);
            }
            switch (tokens[0])
            {
                case "spawn":
                    
                    if (tokens.Length < 4)
                    {
                        Console.WriteLine("Error:: not enough arguments to spawn entity!");
                        return;
                    }

                    var entity = tokens[1];

                    float x, y;

                    bool x_worked = float.TryParse(tokens[2],out x);
                    bool y_worked = float.TryParse(tokens[3],out y);

                    if (!x_worked) {
                        Console.WriteLine("Error:: cannot parse x coord!");
                        return;
                    }

                    if (!y_worked) {
                        Console.WriteLine("Error:: cannot parse y coord!");
                        return;
                    }

                    Entity_Assembler.Get.Create_Entity_From_Table(
                            lua_engine.Get_Table_From_File(Entity_Assembler.Get.Default_Entity_Path + entity + ".lua", ""),
                            new Vector2(x, y)
                        );

                    break;
            }

            command_text = keyboard_handler.tekst = "";
        }

        public void Draw(SpriteBatch batch)
        {
            if (Project_Imagine.PAUSED)
            {
                batch.Draw(rect, new Rectangle(0, 0, Project_Imagine.WIDTH / 2, 32), Color.White);
                batch.DrawString(Content_Manager.Get.Get_Font("sprite_font"), command_text, new Vector2(0, 8), Color.Black);
            }
        }
    }
}
