﻿using Artemis;
using Microsoft.Xna.Framework;
using NLua;
using Project_Imagine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    class Entity_Assembler
    {
        private static Entity_Assembler instance;

        private Lua_Engine lua_engine;
        private EntityWorld world;
        private string default_entity_path = "";

        public string Default_Entity_Path {
            get { return default_entity_path; }
            set { default_entity_path = value; }
        }

        public void Give_Lua_Engine(Lua_Engine engine)
        {
            lua_engine = engine;
        }

        public void Give_Entity_World(EntityWorld _world)
        {
            world = _world;
        }

        public void Set_Default_Entity_Path(string path)
        {
            default_entity_path = path;
        }

        public Entity Create_Entity_From_Table(LuaTable table, Vector2 position)
        {
            if (table == null)
            {
                Console.WriteLine("ERROR:: cannot create entity from an empty table!");
                return null;
            }

            string name = table["name"] as String;
            LuaTable components = table["components"] as LuaTable;

            var entity = world.CreateEntity(Project_Imagine.Get_New_UUID());

            foreach (var elem in components.Keys)
            {
                LuaTable comp = components[elem] as LuaTable;
                string elem_id = elem.ToString();
                if (elem_id == "Body_Component")
                {
                    var w = comp[1] as double?;
                    var h = comp[2] as double?;
                    entity.AddComponent(new Body_Component(position.X, position.Y, (float)w, (float)h));
                }else if (elem_id == "Sprite_Component")
                {
                    var sheet_name = comp[1] as String;
                    var x = comp[2] as double?;
                    var y = comp[3] as double?;
                    var w = comp[4] as double?;
                    var h = comp[5] as double?;
                    entity.AddComponent(new Sprite_Component(sheet_name,
                        new Rectangle(
                            (int)x,
                            (int)y,
                            (int)w,
                            (int)h
                            )));
                }else if(elem_id == "Physics_Component")
                {
                    var physics_id = comp[1] as String;
                    entity.AddComponent(new Physics_Body_Component(physics_id,
                        (self,other)=> { return false; }));
                }else if(elem_id == "Animation_Component")
                {
                    var sprite_sheet_name   = comp[1] as String;
                    var frames              = comp["frames"] as LuaTable;
                    var animations          = comp["animations"] as LuaTable;

                    var _frames = new List<Rectangle>();
                    KeyValuePair<string, Animation>[] _animations = new KeyValuePair<string, Animation>[animations.Keys.Count];

                    foreach (var frame_key in frames.Keys)
                    {
                        LuaTable frame = frames[frame_key] as LuaTable;
                        var x = (int)(frame[1] as Double?);
                        var y = (int)(frame[2] as Double?);
                        var w = (int)(frame[3] as Double?);
                        var h = (int)(frame[4] as Double?);
                        _frames.Add(new Rectangle(x,y,w,h));
                    }

                    foreach (var anim_key in animations.Keys)
                    {
                        var anim = animations[anim_key] as LuaTable;
                        var play_speed = (Double)anim[1] as Double?;
                        //Console.WriteLine("hey");
                        // (TODO): finish this routine to create a animation component

                    }

                }
            }

            return entity;
        }

        private Entity_Assembler() { }
        public static Entity_Assembler Get
        {
            get
            {
                if (instance == null) instance = new Entity_Assembler();
                return instance;
            }
        }
    }
}
