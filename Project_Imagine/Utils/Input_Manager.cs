﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    public class KbHandler
    {
        private Keys[] lastPressedKeys;
        public string tekst = "";

        public bool LControl    = false;
        public bool LShift      = false;

        private List<Func<Keys, bool>> keypressed_callbacks;
        private List<Func<Keys, bool>> keyreleased_callbacks;

        public KbHandler() {
            lastPressedKeys = new Keys[0];
            keypressed_callbacks = new List<Func<Keys, bool>>();
            keyreleased_callbacks = new List<Func<Keys, bool>>();
        }
    
        public void Add_Keypressed_Callback(Func<Keys, bool> callback) {
            keypressed_callbacks.Add(callback);
        }

        public void Add_Keyreleased_Callback(Func<Keys, bool> callback) {
            keyreleased_callbacks.Add(callback);
        }

        public void Update()
        {
            KeyboardState kbState = Keyboard.GetState();
            Keys[] pressedKeys = kbState.GetPressedKeys();

            //check if any of the previous update's keys are no longer pressed
            foreach (Keys key in lastPressedKeys)
                if (!pressedKeys.Contains(key))
                    OnKeyUp(key);

            //check if the currently pressed keys were already pressed
            foreach (Keys key in pressedKeys)
                if (!lastPressedKeys.Contains(key))
                    OnKeyDown(key);

            //save the currently pressed keys so we can compare on the next update
            lastPressedKeys = pressedKeys;
        }

        //Create your own   
        private void OnKeyDown(Keys key)
        {
            foreach (var callback in keypressed_callbacks)
                callback(key);

            switch (key)
            {
                case Keys.D0: tekst += "0"; break;
                case Keys.D1: tekst += "1"; break;
                case Keys.D2: tekst += "2"; break;
                case Keys.D3: tekst += "3"; break;
                case Keys.D4: tekst += "4"; break;
                case Keys.D5: tekst += "5"; break;
                case Keys.D6: tekst += "6"; break;
                case Keys.D7: tekst += "7"; break;
                case Keys.D8: tekst += "8"; break;
                case Keys.D9: tekst += "9"; break;
                case Keys.NumPad0: tekst += "0"; break;
                case Keys.NumPad1: tekst += "1"; break;
                case Keys.NumPad2: tekst += "2"; break;
                case Keys.NumPad3: tekst += "3"; break;
                case Keys.NumPad4: tekst += "4"; break;
                case Keys.NumPad5: tekst += "5"; break;
                case Keys.NumPad6: tekst += "6"; break;
                case Keys.NumPad7: tekst += "7"; break;
                case Keys.NumPad8:tekst += "8"; break;
                case Keys.NumPad9: tekst += "9"; break;
                case Keys.OemPeriod: tekst += "."; break;
                case Keys.Space: tekst += ' '; break;
                case Keys.LeftControl: LControl = true; break;
                case Keys.LeftShift: LShift = true; break;
                case Keys.OemSemicolon: tekst += ';'; break;
                case Keys.Enter: break;
                case Keys.Back:
                    if (tekst.Length > 0) tekst = tekst.Remove(tekst.Length - 1, 1);
                    break;
            }
            if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".Contains(key.ToString()))
            {
                var str = key.ToString().ToLower();
                if (LShift) str = str.ToUpper();
                tekst += str;
            }

        }

        private void OnKeyUp(Keys key)
        {
            foreach (var callback in keyreleased_callbacks)
                callback(key);

            //do stuff
            switch (key)
            {
                case Keys.LeftControl: LControl = false; break;
                case Keys.LeftShift: LShift = false;break;
            }
        }
    }

    enum Game_Keys
    {
        DEBUG = Keys.Q,
        VK_LEFT = Keys.Left,
        VK_RIGHT = Keys.Right,
        VK_UP = Keys.Up,
        VK_DOWN = Keys.Down
    }

    class Input_Manager
    {
        private static Input_Manager instance;

        private bool [] Pressed;

        public bool Player_Left()
        {
            return Keyboard.GetState().IsKeyDown(Keys.Left) ||
                    GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X < 0;
        }

        public bool Player_Right()
        {
            return Keyboard.GetState().IsKeyDown(Keys.Right) ||
                    GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X > 0;
        }

        public bool Player_Up()
        {
            return Keyboard.GetState().IsKeyDown(Keys.Up) ||
                    GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < 0;
        }

        public bool Player_Down()
        {
            return Keyboard.GetState().IsKeyDown(Keys.Down) ||
                    GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0;
        }

        public bool Quit_Button() { return Key_Down(Keys.Escape); }

        public bool Key_Down(Keys key)
        {
            return Keyboard.GetState().IsKeyDown(key);
        }
        
        private Input_Manager()
        {

        }

        public static Input_Manager Get
        {
            get
            {
                if (instance == null) instance = new Input_Manager();
                return instance;
            }
        }
    }
}
