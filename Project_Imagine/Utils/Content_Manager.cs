﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    class Content_Manager
    {
        private static Content_Manager instance;

        private Dictionary<string,Texture2D> textures;
        private Dictionary<string,SpriteFont> fonts;

        private Content_Manager()
        {
            textures = new Dictionary<string, Texture2D>();
            fonts = new Dictionary<string, SpriteFont>();   
        }

        public void Add_Textures(params Texture2D[] textures)
        {
            foreach (var t in textures)
                Add_Texture(t);
        }

        public Texture2D Get_Texture(string id)
        {
            if (textures[id] == null) {
                Console.WriteLine("ERROR::TEXTURE cannot find texture: " + id);
                return null;
            }

            return textures[id];
        }

        public void Add_Texture(Texture2D text)
        {
            textures.Add(text.Name.ToString(), text);
        }

        public void Add_Font(string id,SpriteFont font)
        {
            this.fonts.Add(id, font);
        }

        public SpriteFont Get_Font(string id)
        {
            return fonts[id];
        }

        public static Content_Manager Get {
            get
            {
                if (instance == null) instance = new Content_Manager();
                return instance;
            }
        }

    }
}
