﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    class Camera2D
    {
        readonly Viewport view;

        public Vector2 Position;
        public Vector2 Origin;

        public float Rotation { get; set; }
        public float Zoom { get; set; }

        public Camera2D(Viewport _view)
        {
            view = _view;
            Origin = new Vector2(
                    _view.Width / 2f, _view.Height / 2f
                );

            Position = Vector2.Zero;
            Zoom = 1f;
            Rotation = 0;
        }

        public float Get_Scaled_Width()
        {
            return Project_Imagine.WIDTH / Zoom;
        }

        public float Get_Scaled_Height()
        {
            return Project_Imagine.HEIGHT / Zoom;
        }

        public Matrix Get_View_Matrix()
        {
            return
                Matrix.CreateTranslation(new Vector3(-Position, 0)) *
                Matrix.CreateTranslation(new Vector3(-Origin, 0))   *
                Matrix.CreateRotationZ(Rotation)                    *
                Matrix.CreateScale(Zoom, Zoom, 1)                   *
                Matrix.CreateTranslation(new Vector3(Origin, 0));
        }

    }
}
