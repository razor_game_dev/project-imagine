﻿using NLua;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Utils
{
    class Lua_Engine
    {
        private Lua state;

        public Lua_Engine()
        {
            state = new Lua();
        }

        public LuaTable Get_Table_From_File(string file, string table)
        {

            if (!File.Exists(file))
            {
                Console.WriteLine("ERROR:: cannot find file: {0}",file);
                return null;
            }

            var lua_table = state.DoFile(file)[0] as LuaTable;
            return lua_table;
        }

    }
}
