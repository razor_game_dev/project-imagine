﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Project_Imagine.World;
using Project_Imagine.Utils;
using Artemis;
using Project_Imagine.Components;

namespace Project_Imagine.States
{
    class Level_State : Game_State
    {
        Tiled_Map map;
        Physics_World physics_world;
        
        public Level_State(EntityWorld world, Lua_Engine lua, Physics_World _physics_world) : base("Level")
        {
            physics_world = _physics_world;
            Project_Imagine.Get_Camera().Zoom = 4f;

            var player = world.CreateEntity(Project_Imagine.Get_New_UUID());
            player.AddComponent(new Body_Component(
                    new Vector2(32, 32),
                    new Vector2(8, 24)
                   ));
            //player.AddComponent(new Sprite_Component(
            //        "entity_sheet_1",
            //        new Rectangle(0, 0, 13, 18)
            //    ));

            player.AddComponent(new Animation_Component(
                    "player_sheet",
                    new List<Rectangle>
                    {
                        new Rectangle(4, 0, 8, 24),
                        new Rectangle(18,0,12,24),
                        new Rectangle(36,0,8,24),
                        new Rectangle(50, 0, 12,24),
                        new Rectangle(68,0,8,24),
                        new Rectangle(84,0, 8,24),
                        new Rectangle(100,0,8,24),
                        new Rectangle(116,0,8,24)
                    },
                    new KeyValuePair<string, Animation>[]
                    {
                        new KeyValuePair<string, Animation>("Walk-Right",new Animation(
                            10, new int [] {0,1}
                            )),
                        new KeyValuePair<string, Animation>("Walk-Left",new Animation(
                            10, new int [] {2,3}
                            )),
                        new KeyValuePair<string, Animation>("Walk-Down",new Animation(
                            10, new int [] {4,5}
                            )),
                        new KeyValuePair<string, Animation>("Walk-Up",new Animation(
                            10, new int [] {6,7}
                            ))
                    }
                ));

            player.AddComponent(new Camera_Follow_Component(true));
            player.AddComponent(new Physics_Body_Component(
                    "Player",
                    (self, other) =>
                    {
                        return true;
                    }
                ));
            player.AddComponent(new Player_Controller_Component());

            Entity_Assembler.Get.Create_Entity_From_Table(
                lua.Get_Table_From_File("Entities/player.lua", ""),
                new Vector2(32,32)
                );

            Entity_Assembler.Get.Create_Entity_From_Table(
                    lua.Get_Table_From_File("Entities/test.lua", ""),
                    new Vector2(-32,-32)
                );

        }

        public override void Load()
        {
            map = new Tiled_Map(
                    "Maps/test_map.tmx",
                    Content_Manager.Get.Get_Texture("Tiles_1"),
                    Vector2.Zero,
                    physics_world,
                    false
                );
        }

        public override void Update(GameTime game_time)
        {
            map.Update(game_time);
        }

        public override void Draw(SpriteBatch batch)
        {
            map.Draw(batch);
        }

        public override void Destroy()
        {
        }
    }

}
