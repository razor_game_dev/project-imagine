﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.States
{
    public abstract class Game_State
    {
        public string ID { get; }
        public Game_State(string _id)
        {
            ID = _id;
        }

        public abstract void Load();
        public abstract void Destroy();

        public abstract void Update(GameTime game_time);
        public abstract void Draw(SpriteBatch batch);
    }
}
