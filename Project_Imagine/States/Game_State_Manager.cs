﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.States
{
    // (TODO): allow state transitions with timers

    class Game_State_Manager
    {
        List<Game_State> states;

        public Game_State_Manager()
        {
            states = new List<Game_State>();
        }

        public void Update(GameTime game_time)
        {
            if (states.Count > 0)
                for (var i = 0; i < states.Count(); i++)
                {
                    var state = states[i];
                    if (state != null)
                        state.Update(game_time);
                }
        }

        public void Draw(SpriteBatch batch)
        {
            if (states.Count > 0)
                for (var i = 0; i < states.Count(); i++)
                {
                    var state = states[i];
                    if (state != null)
                        state.Draw(batch);
                }
        }

        public void Goto_Scene(Game_State state)
        {
            if (states.Count() > 0)
            {
                states.Last().Destroy();
                states.RemoveAt(states.Count - 1);
            }

            state.Load();
            states.Add(state);
        }
    }
}
