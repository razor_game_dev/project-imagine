﻿using Artemis;
using Artemis.Interface;
using Artemis.System;
using Microsoft.Xna.Framework;
using Project_Imagine.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Components
{
    class Physics_Body_Component : IComponent
    {
        public Vector2 Velocity;
        public Vector2 Speed;
        public Vector2 Facing;

        public float Rotation_Velocity;

        public float Friction;
        public float Direction;
        public Func<Entity, Entity, bool> Collision_Callback;

        public Physics_Body Physics_Body;

        public float Vel_X
        {
            get { return Velocity.X; }
            set { Velocity = new Vector2(value, Velocity.Y); }
        }

        public float Vel_Y
        {
            get { return Velocity.Y; }
            set { Velocity = new Vector2(Velocity.X, value); }
        }

        public bool Pass_Through { get; }
        public string ID { get; }

        public Physics_Body_Component(string _id, Func<Entity, Entity, bool> _collision_callback)
        {
            ID = _id;
            Collision_Callback  = _collision_callback;
            Velocity            = Vector2.Zero;
            Speed               = new Vector2(10, 10);
            Pass_Through        = false;
            Facing              = new Vector2(1, -1);
            Direction           = 0;
            Friction            = 0.9f;
            Rotation_Velocity   = 0;
        }
    }
}
