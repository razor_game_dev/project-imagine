﻿using Artemis.Interface;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Components
{
    class Body_Component : IComponent
    {

        public Vector2 Position;
        public Vector2 Size;

        public float Rotation;

        public bool Intersecting { get; set; }

        public Vector2 Top_Left_Corner
        {
            get
            {
                var off_x = Width / 2;
                var off_y = Height / 2;

                var cos_r = Math.Cos(Rotation);
                var sin_r = Math.Sin(Rotation);

                var rx = (float)(Mid_X + (off_x * cos_r) - (off_y * sin_r));
                var ry = (float)(Mid_Y + (off_x * sin_r) - (off_y * cos_r));

                return new Vector2(rx, ry);
            }
        }

        public float Cos_R
        { get { return (float)Math.Cos(Rotation); } }
        public float Sin_R
        { get { return (float)Math.Sin(Rotation); } }

        public Vector2 UR
        {
            get
            {
                float ox =  Width / 2;
                float oy = -Height / 2;

                float px = X + Width / 2;
                float py = Y + Height / 2;

                return new Vector2(
                        px + (ox) * Cos_R - (oy) * Sin_R,
                        py + (oy) * Cos_R + (ox) * Sin_R
                    );
            }
        }

        public Vector2 UL
        {
            get
            {
                float ox = -Width  / 2;
                float oy = -Height / 2;

                float px = X + Width / 2;
                float py = Y + Height / 2;

                return new Vector2(
                        px + (ox) * Cos_R - (oy) * Sin_R,
                        py + (oy) * Cos_R + (ox) * Sin_R
                    );
            }
        }

        public Vector2 BR
        {
            get
            {
                float ox = Width / 2;
                float oy = Height / 2;

                float px = X + Width / 2;
                float py = Y + Height / 2;

                return new Vector2(
                        px + (ox) * Cos_R - (oy) * Sin_R,
                        py + (oy) * Cos_R + (ox) * Sin_R
                    );
            }
        }

        public Vector2 BL
        {
            get
            {
                float ox = -Width / 2;
                float oy =  Height / 2;

                float px = X + Width / 2;
                float py = Y + Height / 2;

                return new Vector2(
                        px + (ox) * Cos_R - (oy) * Sin_R,
                        py + (oy) * Cos_R + (ox) * Sin_R
                    );
            }
        }

        public bool Intersects(Body_Component other)
        {
            return (X + Width > other.X) &&
                   (X < other.X + other.Width) &&
                   (Y + Height > other.Y) &&
                   (Y < other.Y + other.Height);
        }
        public float X
        {
            get { return Position.X; }
            set { Position.X = value; }
        }

        public float Y
        {
            get { return Position.Y; }
            set { Position.Y = value; }
        }

        public float Width
        {
            get { return Size.X; }
            set { Size.X = value; }
        }

        public float Height
        {
            get { return Size.Y; }
            set { Size.Y = value; }
        }

        public Vector2 Left
        {
            get {return new Vector2(X + Width, Y); }
        }

        public Vector2 Bottom_Right
        {
            get {return new Vector2(X + Width, Y + Height); }
        }

        public Vector2 Bottom
        {
            get { return new Vector2(X, Y + Height); }
        }

        public float Mid_X
        {
            get { return X + Width / 2; }
        }

        public float Mid_Y
        {
            get { return Y + Height / 2; }
        }

        public void Set(Body_Component _body)
        {
            this.Position = _body.Position;
            this.Size = _body.Size;
        }

        public Vector2 Mid
        {
            get { return new Vector2(this.Mid_X, this.Mid_Y); }
        }

        public Body_Component(float _x, float _y, float _w, float _h)
        {
            X           = _x;
            Y           = _y;
            Width       = _w;
            Height      = _h;
            Rotation    = 0;
        }

        public Body_Component(Vector2 _position, Vector2 _size)
        {
            X           = _position.X;
            Y           = _position.Y;
            Width       = _size.X;
            Height      = _size.Y;
            Rotation    = 0;
        }
    }
}
