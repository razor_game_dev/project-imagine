﻿using Artemis.Interface;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Project_Imagine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Components
{
    class Sprite_Component : IComponent
    {
        public Texture2D Image;
        public Rectangle Region;

        public Vector2 Offset;
        public Vector2 Origin;

        public SpriteEffects Effects;

        public float Layer { get; set; }
        public float Scale { get; set; }
        public Color Fill_Color { get; set; }
        public float Rotation { get; set; }

        public Sprite_Component(string _image_id, Rectangle _region)
        {
            Region      = _region;
            Offset      = Vector2.Zero;
            Layer       = Project_Imagine.ENTITY_LAYER;
            Scale       = 1;
            Fill_Color  = Color.White;
            Image       = Content_Manager.Get.Get_Texture(_image_id);
            Rotation    = 0.0f;
            Origin      = Vector2.Zero;
            Effects     = SpriteEffects.None;
        }

    }
}
