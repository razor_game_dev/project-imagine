﻿using Artemis.Interface;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Imagine.Components
{
    class Camera_Follow_Component : IComponent
    {
        public float Damping { get; set; }
        public float Pan_Speed { get; set; }
        public float Max_Dist { get; set; }
        public float Friction { get; set; }

        public Vector2 Offset;

        public bool Can_Look { get; set; }

        public Camera_Follow_Component(bool _can_look = false)
        {
            Offset      = Vector2.Zero;
            Can_Look    = _can_look;
            Damping     = 0.2f;
            Pan_Speed   = 10f;
            Max_Dist    = 16f;
        }
    }
}
